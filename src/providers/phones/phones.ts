import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';

/*
  Generated class for the PhonesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PhonesProvider {
  private url: string

  constructor(public http: Http) {
    this.url = 'https://api.myjson.com/bins/maaiv'
  }

  phones(): Promise<any[]> {
    return new Promise(resolve => {
      this.http.get(this.url)
        .retry(3)
        .map(res => res.json())
        .subscribe(res => {
          console.log(res)
          resolve(res.phones)
        })
    })
  }

}
