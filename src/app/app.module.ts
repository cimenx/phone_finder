import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TwoColumnListComponent } from '../components/two-column-list/two-column-list';
import { PhonesProvider } from '../providers/phones/phones';
import { HttpModule } from "@angular/http";
import { TurncatePipe } from '../pipes/turncate/turncate';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TwoColumnListComponent,
    TurncatePipe
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PhonesProvider
  ]
})
export class AppModule {}
