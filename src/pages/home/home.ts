import { PhonesProvider } from './../../providers/phones/phones';
import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import * as _ from "lodash";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PhonesProvider]
})
export class HomePage {
  filter_years: string = 'All years'
  filter_brands: string = 'All brands'

  search_text: string = ''
  brands_option: Array<string> = []
  years_option: Array<number> = []

  phones: Array<any>
  cache: Array<any>
  brands: Set<string>
  years: Set<number>

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create({
      content: 'Fetch data...'
    })

    loading.present()

    this.phonesService.phones().then(res => {
      loading.dismiss()
      this.cache = this.phones = res
      for (let phone of this.phones) {
        this.brands.add(phone.brand)
        this.years.add(phone.release_year)
      }
    })
  }

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public phonesService: PhonesProvider) {
    this.phones = []
    this.brands = new Set()
    this.years = new Set()
  }

  filter(ev: any) {
    let search_text = this.search_text.toLowerCase()

    if (
      !search_text && search_text.trim() == '' &&
      this.years_option.length == 0 &&
      this.brands_option.length == 0
    ) {
      this.phones = this.cache
      this.filter_years = 'All years'
      this.filter_brands = 'All brands'
    } else {
      this.phones = _.filter(this.cache, (phone) => {
        let is_search = (phone.name.toLowerCase().indexOf(search_text) != -1)

        let is_year = true
        if (this.years_option.length != 0) {
          is_year = _.includes(this.years_option, phone.release_year)
          this.filter_years = 'Filtered for ' + this.years_option.join(', ')
        } else { this.filter_years = 'All years' }

        let is_brand = true
        if (this.brands_option.length != 0) {
          is_brand = _.includes(this.brands_option, phone.brand)
          this.filter_brands = 'Filtered for ' + this.brands_option.join(', ')
        } else { this.filter_brands = 'All brands' }

        return is_brand && is_year && is_search
      })
    }
  }
}
