Live preview: http://phone-finder.surge.sh

This project build using [Ionic](http://ionicframework.com/docs/).
You need to install `npm` and `ionic` to run project.

There are 2 ways to run this project
## With the NPM:
```bash
npm install
npm run build
```
Then open `www/index.html` in your browser

### With the Ionic CLI:
```bash
ionic serve --lab
```

### Note:
Prior to the bug issues [#12403](https://github.com/ionic-team/ionic/issues/12403) in ionicframework, 
for a moment filter box can only be shown as alert box not dropdown/popover list.
